export default function () {
  this.namespace = '/api';

  this.get('/products', function () {
    return {
      data: [
        {
          type: 'product',
          id: '2',
          attributes: {
            name: 'Strawberries',
            price: 500,
            code: 'SR1',
            imageUrl: 'SR1.png',
            discountText: '3 for £13.50',
            quantity: 0,
          },
        },
        {
          type: 'product',
          id: '3',
          attributes: {
            name: 'Coffee',
            price: 1123,
            code: 'CF1',
            imageUrl: 'CF1.png',
            discountText: 'Multi-buy discount',
            quantity: 0,
          },
        },
        {
          type: 'product',
          id: '1',
          attributes: {
            name: 'Green Tea',
            price: 311,
            code: 'GR1',
            imageUrl: 'GR1.png',
            discountText: '2 for 1',
            quantity: 0,
          },
        },
      ],
    };
  });
}

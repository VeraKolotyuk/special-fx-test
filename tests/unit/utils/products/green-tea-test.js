import { module, test } from 'qunit';
import GreenTea from 'kshop-app/utils/products/green-tea';
import { productTypeCodes } from 'kshop-app/utils/products/codes';

module('Unit | Utility | Green Tea', function () {
  test('it calculate total for single green tea', function (assert) {
    let gt = new GreenTea({code: productTypeCodes.GREEN_TEA, price: 311, quantity: 1});
    assert.equal(gt.total(), 311);
  });

  test('it calculate total for two packs ofgreen tea', function (assert) {
    let gt = new GreenTea({code: productTypeCodes.GREEN_TEA, price: 311, quantity: 2});
    assert.equal(gt.total(), 311);
  });

  test('it calculate total for tree packs of green tea', function (assert) {
    let gt = new GreenTea({code: productTypeCodes.GREEN_TEA, price: 311, quantity: 3});
    assert.equal(gt.total(), 622);
  });

  test('it calculate total for six packs of green tea', function (assert) {
    let gt = new GreenTea({code: productTypeCodes.GREEN_TEA, price: 311, quantity: 6});
    assert.equal(gt.total(), 933);
  });

  test('it calculate total for nine packs of green tea', function (assert) {
    let gt = new GreenTea({code: productTypeCodes.GREEN_TEA, price: 311, quantity: 9});
    assert.equal(gt.total(), 1555);
  });
});

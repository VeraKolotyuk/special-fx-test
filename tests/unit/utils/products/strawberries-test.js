import { module, test } from 'qunit';
import Strawberries from 'kshop-app/utils/products/strawberries';
import { productTypeCodes } from 'kshop-app/utils/products/codes';

module('Unit | Utility | Strawberries', function () {
  test('it calculate total for single strawberry', function (assert) {
    let str = new Strawberries({code: productTypeCodes.STRAWBERRY, price: 500, quantity: 1});
    assert.equal(str.total(), 500);
  });

  test('it calculate total for two strawberry', function (assert) {
    let str = new Strawberries({code: productTypeCodes.STRAWBERRY, price: 500, quantity: 2});
    assert.equal(str.total(), 1000);
  });

  test('it calculate total for tree strawberry', function (assert) {
    let str = new Strawberries({code: productTypeCodes.STRAWBERRY, price: 500, quantity: 3});
    assert.equal(str.total(), 1350);
  });

  test('it calculate total for five strawberry', function (assert) {
    let str = new Strawberries({code: productTypeCodes.STRAWBERRY, price: 500, quantity: 5});
    assert.equal(str.total(), 2250);
  });
});

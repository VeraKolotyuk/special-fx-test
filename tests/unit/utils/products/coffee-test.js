import { module, test } from 'qunit';
import Coffee from 'kshop-app/utils/products/coffee';
import { productTypeCodes } from 'kshop-app/utils/products/codes';

module('Unit | Utility | Coffee', function () {
  test('it calculate total for single coffee', function (assert) {
    let cf = new Coffee({code: productTypeCodes.COFFEE, price: 1123, quantity: 1});
    assert.equal(cf.total(), 1123);
  });

  test('it calculate total for two coffee', function (assert) {
    let cf = new Coffee({code: productTypeCodes.COFFEE, price: 1123, quantity: 2});
    assert.equal(cf.total(), 2246);
  });

  test('it calculate total for tree coffee', function (assert) {
    let cf = new Coffee({code: productTypeCodes.COFFEE, price: 1123, quantity: 3});
    assert.equal(cf.total(), 2246);
  });

  test('it calculate total for five coffee', function (assert) {
    let cf = new Coffee({code: productTypeCodes.COFFEE, price: 1123, quantity: 5});
    assert.equal(cf.total(), 3743.33);
  });
});

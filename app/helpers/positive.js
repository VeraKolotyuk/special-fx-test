import { helper } from '@ember/component/helper';

export function positive(param) {
  return param > 0;
}

export default helper(positive);

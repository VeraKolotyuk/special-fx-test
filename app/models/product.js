import Model, { attr } from '@ember-data/model';
import EmberObject, { computed } from '@ember/object';

export default class ProductModel extends Model {
  @attr('string') name;
  @attr('number') price;
  @attr('string') code;
  @attr('string') imageUrl;
  @attr('string') discountText;
}

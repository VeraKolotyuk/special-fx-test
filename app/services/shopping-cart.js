import { A } from '@ember/array';
import Service from '@ember/service';
import { tracked } from '@glimmer/tracking';
import { computed } from '@ember/object';
import currency from 'currency.js';
import ProductFactory from '../utils/product-factory';

export default class ShoppingCartService extends Service {
  @tracked items = A([]);
  selectedCurrency = '£';

  init() {
    super.init(...arguments);
    this.addObserver('items.@each.quantity', this, () => {
      if (window.localStorage) {
        window.localStorage.setItem('cart', JSON.stringify(this.payload()));
      }
    });
  }

  getProductQuantity(item) {
    return this.items.find((i) => {
      return i.code === item.code;
    }).quantity;
  }

  isLastProduct(item) {
    return this.getProductQuantity(item) === 1;
  }

  add(item) {
    const found = this.items.find((i) => {
      return i.code === item.code;
    });

    if (!found) {
      const product = new ProductFactory().init(item);
      product.set('quantity', 1);
      this.items.pushObject(product);
    } else {
      found.set('quantity', found.quantity + 1);
    }
  }

  remove(item) {
    const found = this.items.find((i) => {
      return i.code === item.code;
    });

    if (found) {
      found.set('quantity', found.quantity - 1);
    }

    if (found.quantity === 0) {
      this.items.removeObject(found);
    }
  }

  payload() {
    return this.items.map((item) => {
      return {
        name: item.name,
        price: item.price,
        quantity: item.quantity,
        imageUrl: item.imageUrl,
        code: item.code,
        discountText: item.discountText,
      };
    });
  }

  count() {
    return this.items.reduce(function (total, item) {
      return total + item.quantity;
    }, 0);
  }

  get totalWithoutDiscount() {
    const twd = this.items.reduce(function (total, item) {
      return currency(item.price).multiply(item.quantity).add(total);
    }, 0);

    return twd;
  }

  get discount() {
    return currency(this.totalWithoutDiscount).subtract(this.total);
  }

  formatPrice(price) {
    return currency(currency(price).divide(100), {
      symbol: this.selectedCurrency,
    }).format();
  }

  productTotal(item) {
    return this.formatPrice(currency(item.price).multiply(item.quantity));
  }

  @computed('items.@each.quantity')
  get total() {
    let total = currency(0);

    this.items.forEach((product) => {
      total = total.add(product.total());
    });

    return total;
  }
}

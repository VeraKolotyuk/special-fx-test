import ProductFactory from '../utils/product-factory';

export function initialize(applicationInstance) {
  let cartService = applicationInstance.lookup('service:shopping-cart');

  let payload;

  if (window.localStorage.getItem('cart')) {
    payload = window.localStorage.getItem('cart');
    payload = JSON.parse(payload);
  }

  if (payload) {
    payload.forEach((item) => {
      cartService.items.pushObject(new ProductFactory().init(item));
    });
  }
}

export default {
  initialize,
};

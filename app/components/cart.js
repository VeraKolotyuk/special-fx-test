import Component from '@glimmer/component';
import { service } from '@ember/service';
import { action } from '@ember/object';
import { tracked } from '@glimmer/tracking';

export default class Cart extends Component {
  @service('shopping-cart') cart;
  @tracked isShowingSummary = false;

  get isShowingSummary() {
    return this.isShowingSummary;
  }

  get total() {
    return this.cart.formatPrice(this.cart.total);
  }

  get totalWithoutDiscount() {
    return this.cart.formatPrice(this.cart.totalWithoutDiscount);
  }

  get discount() {
    return this.cart.formatPrice(this.cart.discount);
  }

  get count() {
    return this.cart.count();
  }

  @action
  toggleSummary() {
    this.isShowingSummary = !this.isShowingSummary;
  }

  @action
  productPrice(product) {
    return this.cart.formatPrice(product.price);
  }

  @action
  productTotal(product) {
    return this.cart.productTotal(product);
  }

  @action
  isLastProduct(product) {
    return this.cart.isLastProduct(product);
  }

  @action
  getProductQuantity(product) {
    return this.cart.getProductQuantity(product);
  }

  @action
  add(item) {
    this.cart.add(item);
  }

  @action
  remove(item) {
    this.cart.remove(item);
  }
}

import Component from '@glimmer/component';
import { action } from '@ember/object';
import { service } from '@ember/service';

export default class ProductComponent extends Component {
  @service('shopping-cart') cart;

  @action
  addToCart(product) {
    this.cart.add(product);
  }

  @action
  removeFromCart(product) {
    this.cart.remove(product);
  }

  @action
  productPrice(product) {
    return this.cart.formatPrice(product.price);
  }

  @action
  isLastProduct(product) {
    const fromCart = this.cart.items.find((i) => {
      return i.code === product.code;
    });
    return fromCart ? this.cart.isLastProduct(fromCart) : false;
  }

  @action
  getProductQuantity(product) {
    const fromCart = this.cart.items.find((i) => {
      return i.code === product.code;
    });
    return fromCart ? this.cart.getProductQuantity(fromCart) : 0;
  }

  @action
  showProductCounter(product) {
    const fromCart = this.cart.items.find((i) => {
      return i.code === product.code;
    });
    return fromCart ? fromCart.quantity > 0 : false;
  }
}

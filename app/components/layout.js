import Component from '@glimmer/component';
import { service } from '@ember/service';

export default class LayoutComponent extends Component {
  @service('shopping-cart') cart;

  get count() {
    return this.cart.count();
  }

  get total() {
    return this.cart.formatPrice(this.cart.total);
  }
}

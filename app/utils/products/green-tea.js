import BuyForPrice from '../discounts/buy-for-price';
import BuyOneGetOneFree from '../discounts/buy-one-get-one-free';
import CartProduct from './cart-product';

class GeenTea extends CartProduct {
  constructor() {
    super(...arguments);
  }

  total() {
    if (this.quantity >= 2) {
      return new BuyOneGetOneFree().total(this);
    } else {
      return new BuyForPrice().total(this);
    }
  }
}

export default GeenTea;

import BuyForPrice from '../discounts/buy-for-price';
import MultiBuyDiscount from '../discounts/multi-buy-discount';
import CartProduct from './cart-product';

class Strawberries extends CartProduct {
  constructor() {
    super(...arguments);
  }

  total() {
    if (this.quantity >= 3) {
      return new MultiBuyDiscount(10).total(this);
    } else {
      return new BuyForPrice().total(this);
    }
  }
}

export default Strawberries;

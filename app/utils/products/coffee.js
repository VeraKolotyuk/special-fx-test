import BuyForPrice from '../discounts/buy-for-price';
import TwoThirdsOfPrice from '../discounts/two-thirds-of-price';
import CartProduct from './cart-product';

class Coffee extends CartProduct {
  constructor() {
    super(...arguments);
  }

  total() {
    if (this.quantity >= 3) {
      return new TwoThirdsOfPrice().total(this);
    } else {
      return new BuyForPrice().total(this);
    }
  }
}

export default Coffee;

import { tracked } from '@glimmer/tracking';
import BuyForPrice from '../discounts/buy-for-price';

class CartProduct {
  @tracked quantity;

  constructor(item) {
    this.name = item.name;
    this.quantity = item.quantity;
    this.code = item.code;
    this.price = item.price;
    this.imageUrl = item.imageUrl;
    this.discountText = item.discountText;
  }

  set(property, value) {
    this[property] = value;
  }

  total() {
    return new BuyForPrice().total(this);
  }
}

export default CartProduct;

export const productTypeCodes = {
  GREEN_TEA: 'GR1',
  STRAWBERRY: 'SR1',
  COFFEE: 'CF1',
};

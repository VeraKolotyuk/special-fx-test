import CartProduct from './products/cart-product';
import GreenTea from './products/green-tea';
import Strawberries from './products/strawberries';
import Coffee from './products/coffee';
import { productTypeCodes } from './products/codes';

class ProductFactory {
  init(product) {
    let instance = null;
    if (product.code === productTypeCodes.GREEN_TEA) {
      return new GreenTea(product);
    }
    if (product.code === productTypeCodes.STRAWBERRY) {
      return new Strawberries(product);
    }
    if (product.code === productTypeCodes.COFFEE) {
      return new Coffee(product);
    } else {
      instance = new CartProduct(product);
    }
    return instance;
  }
}

export default ProductFactory;

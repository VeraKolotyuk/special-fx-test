import currency from 'currency.js';

class TwoThirdsOfPrice {
  total(product) {
    return currency(product.price).multiply(product.quantity).multiply(2).divide(3);
  }
}

export default TwoThirdsOfPrice;

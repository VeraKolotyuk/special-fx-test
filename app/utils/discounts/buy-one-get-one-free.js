import currency from 'currency.js';

class BuyOneGetOneFree {
  total(product) {
    const amount = Math.ceil(product.quantity / 2);
    return currency(product.price).multiply(amount);
  }
}

export default BuyOneGetOneFree;

import currency from 'currency.js';

class BuyForPrice {
  total(product) {
    return currency(product.price).multiply(product.quantity);
  }
}

export default BuyForPrice;

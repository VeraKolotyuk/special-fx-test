import currency from 'currency.js';

class MultiBuyDiscount {
  constructor(discountPercent) {
    this.discountPercent = discountPercent;
  }

  total(product) {
    return currency((100 - this.discountPercent) * product.price).divide(100).multiply(product.quantity);
  }
}

export default MultiBuyDiscount;
